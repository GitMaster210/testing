#!/bin/bash

if [ -f "/etc/arch-release" ] || [ -f "/etc/artix-release" ]; then
	yes "" | sudo pacman -Sy git neovim xclip python3 python-pip nodejs npm gcc ranger fzf ripgrep the_silver_searcher fd tree-sitter lazygit || exit 126
else # assume debian based
        sudo apt install -y git xclip python3 python3-pip nodejs npm gcc ranger fzf ripgrep silversearcher-ag fd-find libtree-sitter-dev || exit 126
	LAZYGIT_VERSION=$(curl -s "https://api.github.com/repos/jesseduffield/lazygit/releases/latest" | grep -Po '"tag_name": "v\K[^"]*') || exit 126
        curl -Lo lazygit.tar.gz "https://github.com/jesseduffield/lazygit/releases/latest/download/lazygit_${LAZYGIT_VERSION}_Linux_x86_64.tar.gz" || exit 126
        tar xf lazygit.tar.gz lazygit || exit 126
        sudo install lazygit /usr/local/bin || exit 126
	git clone https://gitlab.com/GitMaster210/extra-files-for-magicvim
	cd ~/extra-files-for-magicvim/utils || exit 126
	sha256sum ~/extra-files-for-magicvim/utils/nvim-linux64.deb || exit 126
	sudo apt install ./nvim-linux64.deb || exit 126
	cd ~ || exit 127
fi

pip install pynvim || exit 127

mkdir ~/.cache/nvim/undo
mkdir ~/.config/nvim
mkdir ~/.local/share/nvim

mv ~/.config/nvim ~/.config/nvim.bak || exit 127
mv ~/.local/share/nvim ~/.local/share/nvim.bak || exit 127

git clone https://gitlab.com/GitMaster210/magicvim ~/.config/nvim
nvim || exit 127
